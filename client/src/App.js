import './styles/App.css';
import twitterLogo from './assets/twitter-logo.svg';
import React, { useEffect, useState } from "react";
import { contractAddress, abi, TWITTER_LINK, OPENSEA_LINK, TOTAL_MINT_COUNT, TWITTER_HANDLE, BUILDSPACE_TWITTER_HANDLE, BUILDSPACE_TWITTER_LINK} from './utils/constants';
import {ethers} from "ethers";

const {ethereum} = window;

const App = () => {

  const [currentAccount, setCurrentAccount] = useState(""); 
  const [isMinting, setIsMinting] = useState(false);
  const [totalNftMinted, setTotalNftMinted] = useState(0);
  const [chainId, setChainID] = useState('0x');

  const renderNotConnectedContainer = () => (
    <button onClick={() => connectWallet()} className="cta-button connect-wallet-button">
      Connect to Wallet
    </button>
  );

  const renderMintNFTContainer = () => (
    <button onClick={() => mintNFT()} className="cta-button connect-wallet-button" disabled={isMinting}>
      {isMinting ? "Minting Please Wait..." : "Mint NFT"}
    </button>
  )

  const getProvider = () => {
    return new ethers.providers.Web3Provider(ethereum);
  } 

  const getContract = () => {
    return new ethers.Contract(contractAddress, abi, getProvider().getSigner());
  }

  const checkIfWalletConnnected = async () => {
    const accounts = await ethereum.request({method: "eth_accounts"});
    setCurrentAccount(accounts.length > 0 ? accounts[0] : '');
  }

  const connectWallet = async () => {
    const accounts = await ethereum.request({method: "eth_requestAccounts"});
    setCurrentAccount(accounts.length > 0 ? accounts[0] : '');
    getTotalNFTMinted();
  }

  const mintNFT = async () => {
    console.log("Going to pop wallet now to pay gas...")
    setIsMinting(true);
    let txn = await getContract().makeNFT();
    console.log("Mining...please wait.")
    await txn.wait();
    setIsMinting(false);
    console.log(`Mined, see transaction: https://rinkeby.etherscan.io/tx/${txn.hash}`);
    getTotalNFTMinted();
  }

  const setUpEventListener = async () => {
      getContract().on('newNFTMinted', (from, tokenId) => {
        let link = `https://testnets.opensea.io/assets/${contractAddress}/${tokenId.toNumber()}`;
        alert("Congrats, Your NFT is minted to your wallet. Link - " + link)
      });
  }

  const getTotalNFTMinted = async () => {
      setTotalNftMinted(await getContract().getTotalNFTMinted());
  }

  const checkForRinkebyChain = async () => {
    setChainID(await ethereum.request({ method: 'eth_chainId' }));
  }

  useEffect(() => {
    
    if(!ethereum){
       alert("Install Metamask");
       return;
    }

    checkForRinkebyChain();

    if(chainId !== '0x4') return;

    checkIfWalletConnnected();

    setUpEventListener();
    
    getTotalNFTMinted();
  }, [chainId])

  return (
    <div className="App">
      <div className="container">
        <div className="header-container">
          <p className="header gradient-text">Gowtham's NFT Collection</p>
          <p className="sub-text">
            Each unique. Each beautiful. Discover your NFT today.
          </p>
          { currentAccount !== "" && <p className="sub-text">
            Total NFT's Minted - <span className='text-red'>{totalNftMinted} </span> / {TOTAL_MINT_COUNT}  
            <br/>( <span className='text-red'>Hurry up!! Only {TOTAL_MINT_COUNT - totalNftMinted} available</span> )
          </p> }
          { ethereum && chainId != '0x4' && <label className='network-msg'>Switch to rinkeby network to start minting !!</label> }
          { ethereum && chainId =='0x4' && currentAccount == "" && renderNotConnectedContainer()}
          { ethereum && chainId =='0x4' && currentAccount != "" && renderMintNFTContainer()}

          <div className='opensea-link'>
            <a href={OPENSEA_LINK} target="_blank" >View Our Collection on OpenSea</a>
          </div>
        </div>
        <div className="footer-container">
          <img alt="Twitter Logo" className="twitter-logo footer-item" src={twitterLogo} />
          <label>Built by</label>
          <a
            className="footer-text footer-item"
            href={TWITTER_LINK}
            target="_blank"
            rel="noreferrer"
          >{` @${TWITTER_HANDLE}`}</a>
          <label>on</label>
          <a
            className="footer-text footer-item"
            href={BUILDSPACE_TWITTER_LINK}
            target="_blank"
            rel="noreferrer"
          >{`@${BUILDSPACE_TWITTER_HANDLE}`}</a>
         
        </div>
      </div>
    </div>
  );
};

export default App;
