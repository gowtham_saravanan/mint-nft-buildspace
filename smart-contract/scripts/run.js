const deployContract = async () => {
    const contractFactory = await hre.ethers.getContractFactory('MyEpicNFT');
    const contract = await contractFactory.deploy();
    await contract.deployed();

    console.log("The NFT contract deployed to - " + contract.address);

}

const main = async () => {
    try{
        await deployContract();
        process.exit(0);
    } catch(error){
        console.log(error);
        process.exit(1);
    }
}

main();